boto3
urllib3
botocore
idna
typing-extensions
requests
setuptools
charset-normalizer
certifi
s3transfer
six
python-dateutil
click
cryptography
pyyaml
jinja2
google-api-core
attrs
numpy
markupsafe
